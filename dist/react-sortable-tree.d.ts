/// <reference types="react" />
import './react-sortable-tree.css';
import { ReactSortableTreeProps } from './utils/default-handlers';
declare const SortableTreeWithoutDndContext: (props: ReactSortableTreeProps) => JSX.Element;
declare const SortableTree: (props: ReactSortableTreeProps) => JSX.Element;
export { SortableTreeWithoutDndContext };
export default SortableTree;
