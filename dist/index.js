'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var jsxRuntime = require('react/jsx-runtime');
var React = require('react');
var withScrolling = require('@nosferatu500/react-dnd-scrollzone');
var isEqual = require('lodash.isequal');
var reactDnd = require('react-dnd');
var reactDndHtml5Backend = require('react-dnd-html5-backend');
var reactVirtuoso = require('react-virtuoso');
var reactDom = require('react-dom');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var React__default = /*#__PURE__*/_interopDefaultLegacy(React);
var withScrolling__default = /*#__PURE__*/_interopDefaultLegacy(withScrolling);
var isEqual__default = /*#__PURE__*/_interopDefaultLegacy(isEqual);

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _inheritsLoose(subClass, superClass) {
  subClass.prototype = Object.create(superClass.prototype);
  subClass.prototype.constructor = subClass;

  _setPrototypeOf(subClass, superClass);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

var classnames = function classnames() {
  for (var _len = arguments.length, classes = new Array(_len), _key = 0; _key < _len; _key++) {
    classes[_key] = arguments[_key];
  }

  return classes.filter(Boolean).join(' ');
};

var getNodeDataAtTreeIndexOrNextIndex = function getNodeDataAtTreeIndexOrNextIndex(_ref) {
  var targetIndex = _ref.targetIndex,
      node = _ref.node,
      currentIndex = _ref.currentIndex,
      getNodeKey = _ref.getNodeKey,
      _ref$path = _ref.path,
      path = _ref$path === void 0 ? [] : _ref$path,
      _ref$lowerSiblingCoun = _ref.lowerSiblingCounts,
      lowerSiblingCounts = _ref$lowerSiblingCoun === void 0 ? [] : _ref$lowerSiblingCoun,
      _ref$ignoreCollapsed = _ref.ignoreCollapsed,
      ignoreCollapsed = _ref$ignoreCollapsed === void 0 ? true : _ref$ignoreCollapsed,
      _ref$isPseudoRoot = _ref.isPseudoRoot,
      isPseudoRoot = _ref$isPseudoRoot === void 0 ? false : _ref$isPseudoRoot;
  var selfPath = !isPseudoRoot ? [].concat(path, [getNodeKey({
    node: node,
    treeIndex: currentIndex
  })]) : [];

  if (currentIndex === targetIndex) {
    return {
      node: node,
      lowerSiblingCounts: lowerSiblingCounts,
      path: selfPath
    };
  }

  if (!node.children || ignoreCollapsed && node.expanded !== true) {
    return {
      nextIndex: currentIndex + 1
    };
  }

  var childIndex = currentIndex + 1;
  var childCount = node.children.length;

  for (var i = 0; i < childCount; i += 1) {
    var result = getNodeDataAtTreeIndexOrNextIndex({
      ignoreCollapsed: ignoreCollapsed,
      getNodeKey: getNodeKey,
      targetIndex: targetIndex,
      node: node.children[i],
      currentIndex: childIndex,
      lowerSiblingCounts: [].concat(lowerSiblingCounts, [childCount - i - 1]),
      path: selfPath
    });

    if (result.node) {
      return result;
    }

    childIndex = result.nextIndex;
  }

  return {
    nextIndex: childIndex
  };
};

var getDescendantCount = function getDescendantCount(_ref2) {
  var node = _ref2.node,
      _ref2$ignoreCollapsed = _ref2.ignoreCollapsed,
      ignoreCollapsed = _ref2$ignoreCollapsed === void 0 ? true : _ref2$ignoreCollapsed;
  return getNodeDataAtTreeIndexOrNextIndex({
    getNodeKey: function getNodeKey() {},
    ignoreCollapsed: ignoreCollapsed,
    node: node,
    currentIndex: 0,
    targetIndex: -1
  }).nextIndex - 1;
};

var walkDescendants = function walkDescendants(_ref3) {
  var callback = _ref3.callback,
      getNodeKey = _ref3.getNodeKey,
      ignoreCollapsed = _ref3.ignoreCollapsed,
      _ref3$isPseudoRoot = _ref3.isPseudoRoot,
      isPseudoRoot = _ref3$isPseudoRoot === void 0 ? false : _ref3$isPseudoRoot,
      node = _ref3.node,
      _ref3$parentNode = _ref3.parentNode,
      parentNode = _ref3$parentNode === void 0 ? null : _ref3$parentNode,
      currentIndex = _ref3.currentIndex,
      _ref3$path = _ref3.path,
      path = _ref3$path === void 0 ? [] : _ref3$path,
      _ref3$lowerSiblingCou = _ref3.lowerSiblingCounts,
      lowerSiblingCounts = _ref3$lowerSiblingCou === void 0 ? [] : _ref3$lowerSiblingCou;
  var selfPath = isPseudoRoot ? [] : [].concat(path, [getNodeKey({
    node: node,
    treeIndex: currentIndex
  })]);
  var selfInfo = isPseudoRoot ? null : {
    node: node,
    parentNode: parentNode,
    path: selfPath,
    lowerSiblingCounts: lowerSiblingCounts,
    treeIndex: currentIndex
  };

  if (!isPseudoRoot) {
    var callbackResult = callback(selfInfo);

    if (callbackResult === false) {
      return false;
    }
  }

  if (!node.children || node.expanded !== true && ignoreCollapsed && !isPseudoRoot) {
    return currentIndex;
  }

  var childIndex = currentIndex;
  var childCount = node.children.length;

  if (typeof node.children !== 'function') {
    for (var i = 0; i < childCount; i += 1) {
      childIndex = walkDescendants({
        callback: callback,
        getNodeKey: getNodeKey,
        ignoreCollapsed: ignoreCollapsed,
        node: node.children[i],
        parentNode: isPseudoRoot ? null : node,
        currentIndex: childIndex + 1,
        lowerSiblingCounts: [].concat(lowerSiblingCounts, [childCount - i - 1]),
        path: selfPath
      });

      if (childIndex === false) {
        return false;
      }
    }
  }

  return childIndex;
};

var mapDescendants = function mapDescendants(_ref4) {
  var callback = _ref4.callback,
      getNodeKey = _ref4.getNodeKey,
      ignoreCollapsed = _ref4.ignoreCollapsed,
      _ref4$isPseudoRoot = _ref4.isPseudoRoot,
      isPseudoRoot = _ref4$isPseudoRoot === void 0 ? false : _ref4$isPseudoRoot,
      node = _ref4.node,
      _ref4$parentNode = _ref4.parentNode,
      parentNode = _ref4$parentNode === void 0 ? null : _ref4$parentNode,
      currentIndex = _ref4.currentIndex,
      _ref4$path = _ref4.path,
      path = _ref4$path === void 0 ? [] : _ref4$path,
      _ref4$lowerSiblingCou = _ref4.lowerSiblingCounts,
      lowerSiblingCounts = _ref4$lowerSiblingCou === void 0 ? [] : _ref4$lowerSiblingCou;

  var nextNode = _extends({}, node);

  var selfPath = isPseudoRoot ? [] : [].concat(path, [getNodeKey({
    node: nextNode,
    treeIndex: currentIndex
  })]);
  var selfInfo = {
    node: nextNode,
    parentNode: parentNode,
    path: selfPath,
    lowerSiblingCounts: lowerSiblingCounts,
    treeIndex: currentIndex
  };

  if (!nextNode.children || nextNode.expanded !== true && ignoreCollapsed && !isPseudoRoot) {
    return {
      treeIndex: currentIndex,
      node: callback(selfInfo)
    };
  }

  var childIndex = currentIndex;
  var childCount = nextNode.children.length;

  if (typeof nextNode.children !== 'function') {
    nextNode.children = nextNode.children.map(function (child, i) {
      var mapResult = mapDescendants({
        callback: callback,
        getNodeKey: getNodeKey,
        ignoreCollapsed: ignoreCollapsed,
        node: child,
        parentNode: isPseudoRoot ? null : nextNode,
        currentIndex: childIndex + 1,
        lowerSiblingCounts: [].concat(lowerSiblingCounts, [childCount - i - 1]),
        path: selfPath
      });
      childIndex = mapResult.treeIndex;
      return mapResult.node;
    });
  }

  return {
    node: callback(selfInfo),
    treeIndex: childIndex
  };
};

var getVisibleNodeCount = function getVisibleNodeCount(_ref5) {
  var treeData = _ref5.treeData;

  var traverse = function traverse(node) {
    if (!node.children || node.expanded !== true || typeof node.children === 'function') {
      return 1;
    }

    return 1 + node.children.reduce(function (total, currentNode) {
      return total + traverse(currentNode);
    }, 0);
  };

  return treeData.reduce(function (total, currentNode) {
    return total + traverse(currentNode);
  }, 0);
};
var getVisibleNodeInfoAtIndex = function getVisibleNodeInfoAtIndex(_ref6) {
  var treeData = _ref6.treeData,
      targetIndex = _ref6.index,
      getNodeKey = _ref6.getNodeKey;

  if (!treeData || treeData.length < 1) {
    return null;
  }

  var result = getNodeDataAtTreeIndexOrNextIndex({
    targetIndex: targetIndex,
    getNodeKey: getNodeKey,
    node: {
      children: treeData,
      expanded: true
    },
    currentIndex: -1,
    path: [],
    lowerSiblingCounts: [],
    isPseudoRoot: true
  });

  if (result.node) {
    return result;
  }

  return null;
};
var walk = function walk(_ref7) {
  var treeData = _ref7.treeData,
      getNodeKey = _ref7.getNodeKey,
      callback = _ref7.callback,
      _ref7$ignoreCollapsed = _ref7.ignoreCollapsed,
      ignoreCollapsed = _ref7$ignoreCollapsed === void 0 ? true : _ref7$ignoreCollapsed;

  if (!treeData || treeData.length < 1) {
    return;
  }

  walkDescendants({
    callback: callback,
    getNodeKey: getNodeKey,
    ignoreCollapsed: ignoreCollapsed,
    isPseudoRoot: true,
    node: {
      children: treeData
    },
    currentIndex: -1,
    path: [],
    lowerSiblingCounts: []
  });
};
var map = function map(_ref8) {
  var treeData = _ref8.treeData,
      getNodeKey = _ref8.getNodeKey,
      callback = _ref8.callback,
      _ref8$ignoreCollapsed = _ref8.ignoreCollapsed,
      ignoreCollapsed = _ref8$ignoreCollapsed === void 0 ? true : _ref8$ignoreCollapsed;

  if (!treeData || treeData.length < 1) {
    return [];
  }

  return mapDescendants({
    callback: callback,
    getNodeKey: getNodeKey,
    ignoreCollapsed: ignoreCollapsed,
    isPseudoRoot: true,
    node: {
      children: treeData
    },
    currentIndex: -1,
    path: [],
    lowerSiblingCounts: []
  }).node.children;
};
var toggleExpandedForAll = function toggleExpandedForAll(_ref9) {
  var treeData = _ref9.treeData,
      _ref9$expanded = _ref9.expanded,
      expanded = _ref9$expanded === void 0 ? true : _ref9$expanded;
  return map({
    treeData: treeData,
    callback: function callback(_ref10) {
      var node = _ref10.node;
      return _extends({}, node, {
        expanded: expanded
      });
    },
    getNodeKey: function getNodeKey(_ref11) {
      var treeIndex = _ref11.treeIndex;
      return treeIndex;
    },
    ignoreCollapsed: false
  });
};
var changeNodeAtPath = function changeNodeAtPath(_ref12) {
  var treeData = _ref12.treeData,
      path = _ref12.path,
      newNode = _ref12.newNode,
      getNodeKey = _ref12.getNodeKey,
      _ref12$ignoreCollapse = _ref12.ignoreCollapsed,
      ignoreCollapsed = _ref12$ignoreCollapse === void 0 ? true : _ref12$ignoreCollapse;
  var RESULT_MISS = 'RESULT_MISS';

  var traverse = function traverse(_ref13) {
    var _ref13$isPseudoRoot = _ref13.isPseudoRoot,
        isPseudoRoot = _ref13$isPseudoRoot === void 0 ? false : _ref13$isPseudoRoot,
        node = _ref13.node,
        currentTreeIndex = _ref13.currentTreeIndex,
        pathIndex = _ref13.pathIndex;

    if (!isPseudoRoot && getNodeKey({
      node: node,
      treeIndex: currentTreeIndex
    }) !== path[pathIndex]) {
      return RESULT_MISS;
    }

    if (pathIndex >= path.length - 1) {
      return typeof newNode === 'function' ? newNode({
        node: node,
        treeIndex: currentTreeIndex
      }) : newNode;
    }

    if (!node.children) {
      throw new Error('Path referenced children of node with no children.');
    }

    var nextTreeIndex = currentTreeIndex + 1;

    for (var i = 0; i < node.children.length; i += 1) {
      var _result = traverse({
        node: node.children[i],
        currentTreeIndex: nextTreeIndex,
        pathIndex: pathIndex + 1
      });

      if (_result !== RESULT_MISS) {
        if (_result) {
          return _extends({}, node, {
            children: [].concat(node.children.slice(0, i), [_result], node.children.slice(i + 1))
          });
        }

        return _extends({}, node, {
          children: [].concat(node.children.slice(0, i), node.children.slice(i + 1))
        });
      }

      nextTreeIndex += 1 + getDescendantCount({
        node: node.children[i],
        ignoreCollapsed: ignoreCollapsed
      });
    }

    return RESULT_MISS;
  };

  var result = traverse({
    node: {
      children: treeData
    },
    currentTreeIndex: -1,
    pathIndex: -1,
    isPseudoRoot: true
  });

  if (result === RESULT_MISS) {
    throw new Error('No node found at the given path.');
  }

  return result.children;
};
var removeNodeAtPath = function removeNodeAtPath(_ref14) {
  var treeData = _ref14.treeData,
      path = _ref14.path,
      getNodeKey = _ref14.getNodeKey,
      _ref14$ignoreCollapse = _ref14.ignoreCollapsed,
      ignoreCollapsed = _ref14$ignoreCollapse === void 0 ? true : _ref14$ignoreCollapse;
  return changeNodeAtPath({
    treeData: treeData,
    path: path,
    getNodeKey: getNodeKey,
    ignoreCollapsed: ignoreCollapsed,
    newNode: null
  });
};
var removeNode = function removeNode(_ref15) {
  var treeData = _ref15.treeData,
      path = _ref15.path,
      getNodeKey = _ref15.getNodeKey,
      _ref15$ignoreCollapse = _ref15.ignoreCollapsed,
      ignoreCollapsed = _ref15$ignoreCollapse === void 0 ? true : _ref15$ignoreCollapse;
  var removedNode = null;
  var removedTreeIndex = null;
  var nextTreeData = changeNodeAtPath({
    treeData: treeData,
    path: path,
    getNodeKey: getNodeKey,
    ignoreCollapsed: ignoreCollapsed,
    newNode: function newNode(_ref16) {
      var node = _ref16.node,
          treeIndex = _ref16.treeIndex;
      removedNode = node;
      removedTreeIndex = treeIndex;
      return null;
    }
  });
  return {
    treeData: nextTreeData,
    node: removedNode,
    treeIndex: removedTreeIndex
  };
};
var getNodeAtPath = function getNodeAtPath(_ref17) {
  var treeData = _ref17.treeData,
      path = _ref17.path,
      getNodeKey = _ref17.getNodeKey,
      _ref17$ignoreCollapse = _ref17.ignoreCollapsed,
      ignoreCollapsed = _ref17$ignoreCollapse === void 0 ? true : _ref17$ignoreCollapse;
  var foundNodeInfo = null;

  try {
    changeNodeAtPath({
      treeData: treeData,
      path: path,
      getNodeKey: getNodeKey,
      ignoreCollapsed: ignoreCollapsed,
      newNode: function newNode(_ref18) {
        var node = _ref18.node,
            treeIndex = _ref18.treeIndex;
        foundNodeInfo = {
          node: node,
          treeIndex: treeIndex
        };
        return node;
      }
    });
  } catch (err) {}

  return foundNodeInfo;
};
var addNodeUnderParent = function addNodeUnderParent(_ref19) {
  var treeData = _ref19.treeData,
      newNode = _ref19.newNode,
      _ref19$parentKey = _ref19.parentKey,
      parentKey = _ref19$parentKey === void 0 ? null : _ref19$parentKey,
      getNodeKey = _ref19.getNodeKey,
      _ref19$ignoreCollapse = _ref19.ignoreCollapsed,
      ignoreCollapsed = _ref19$ignoreCollapse === void 0 ? true : _ref19$ignoreCollapse,
      _ref19$expandParent = _ref19.expandParent,
      expandParent = _ref19$expandParent === void 0 ? false : _ref19$expandParent,
      _ref19$addAsFirstChil = _ref19.addAsFirstChild,
      addAsFirstChild = _ref19$addAsFirstChil === void 0 ? false : _ref19$addAsFirstChil;

  if (parentKey === null) {
    return addAsFirstChild ? {
      treeData: [newNode].concat(treeData || []),
      treeIndex: 0
    } : {
      treeData: [].concat(treeData || [], [newNode]),
      treeIndex: (treeData || []).length
    };
  }

  var insertedTreeIndex = null;
  var hasBeenAdded = false;
  var changedTreeData = map({
    treeData: treeData,
    getNodeKey: getNodeKey,
    ignoreCollapsed: ignoreCollapsed,
    callback: function callback(_ref20) {
      var node = _ref20.node,
          treeIndex = _ref20.treeIndex,
          path = _ref20.path;
      var key = path ? path[path.length - 1] : null;

      if (hasBeenAdded || key !== parentKey) {
        return node;
      }

      hasBeenAdded = true;

      var parentNode = _extends({}, node);

      if (expandParent) {
        parentNode.expanded = true;
      }

      if (!parentNode.children) {
        insertedTreeIndex = treeIndex + 1;
        return _extends({}, parentNode, {
          children: [newNode]
        });
      }

      if (typeof parentNode.children === 'function') {
        throw new Error('Cannot add to children defined by a function');
      }

      var nextTreeIndex = treeIndex + 1;

      for (var i = 0; i < parentNode.children.length; i += 1) {
        nextTreeIndex += 1 + getDescendantCount({
          node: parentNode.children[i],
          ignoreCollapsed: ignoreCollapsed
        });
      }

      insertedTreeIndex = nextTreeIndex;
      var children = addAsFirstChild ? [newNode].concat(parentNode.children) : [].concat(parentNode.children, [newNode]);
      return _extends({}, parentNode, {
        children: children
      });
    }
  });

  if (!hasBeenAdded) {
    throw new Error('No node found with the given key.');
  }

  return {
    treeData: changedTreeData,
    treeIndex: insertedTreeIndex
  };
};

var addNodeAtDepthAndIndex = function addNodeAtDepthAndIndex(_ref21) {
  var targetDepth = _ref21.targetDepth,
      minimumTreeIndex = _ref21.minimumTreeIndex,
      newNode = _ref21.newNode,
      ignoreCollapsed = _ref21.ignoreCollapsed,
      expandParent = _ref21.expandParent,
      _ref21$isPseudoRoot = _ref21.isPseudoRoot,
      isPseudoRoot = _ref21$isPseudoRoot === void 0 ? false : _ref21$isPseudoRoot,
      isLastChild = _ref21.isLastChild,
      node = _ref21.node,
      currentIndex = _ref21.currentIndex,
      currentDepth = _ref21.currentDepth,
      getNodeKey = _ref21.getNodeKey,
      _ref21$path = _ref21.path,
      path = _ref21$path === void 0 ? [] : _ref21$path;

  var selfPath = function selfPath(n) {
    return isPseudoRoot ? [] : [].concat(path, [getNodeKey({
      node: n,
      treeIndex: currentIndex
    })]);
  };

  if (currentIndex >= minimumTreeIndex - 1 || isLastChild && !(node.children && node.children.length)) {
    if (typeof node.children === 'function') {
      throw new Error('Cannot add to children defined by a function');
    } else {
      var extraNodeProps = expandParent ? {
        expanded: true
      } : {};

      var _nextNode = _extends({}, node, extraNodeProps, {
        children: node.children ? [newNode].concat(node.children) : [newNode]
      });

      return {
        node: _nextNode,
        nextIndex: currentIndex + 2,
        insertedTreeIndex: currentIndex + 1,
        parentPath: selfPath(_nextNode),
        parentNode: isPseudoRoot ? null : _nextNode
      };
    }
  }

  if (currentDepth >= targetDepth - 1) {
    if (!node.children || typeof node.children === 'function' || node.expanded !== true && ignoreCollapsed && !isPseudoRoot) {
      return {
        node: node,
        nextIndex: currentIndex + 1
      };
    }

    var _childIndex = currentIndex + 1;

    var _insertedTreeIndex = null;
    var insertIndex = null;

    for (var i = 0; i < node.children.length; i += 1) {
      if (_childIndex >= minimumTreeIndex) {
        _insertedTreeIndex = _childIndex;
        insertIndex = i;
        break;
      }

      _childIndex += 1 + getDescendantCount({
        node: node.children[i],
        ignoreCollapsed: ignoreCollapsed
      });
    }

    if (insertIndex === null) {
      if (_childIndex < minimumTreeIndex && !isLastChild) {
        return {
          node: node,
          nextIndex: _childIndex
        };
      }

      _insertedTreeIndex = _childIndex;
      insertIndex = node.children.length;
    }

    var _nextNode2 = _extends({}, node, {
      children: [].concat(node.children.slice(0, insertIndex), [newNode], node.children.slice(insertIndex))
    });

    return {
      node: _nextNode2,
      nextIndex: _childIndex,
      insertedTreeIndex: _insertedTreeIndex,
      parentPath: selfPath(_nextNode2),
      parentNode: isPseudoRoot ? null : _nextNode2
    };
  }

  if (!node.children || typeof node.children === 'function' || node.expanded !== true && ignoreCollapsed && !isPseudoRoot) {
    return {
      node: node,
      nextIndex: currentIndex + 1
    };
  }

  var insertedTreeIndex = null;
  var pathFragment = null;
  var parentNode = null;
  var childIndex = currentIndex + 1;
  var newChildren = node.children;

  if (typeof newChildren !== 'function') {
    newChildren = newChildren.map(function (child, i) {
      if (insertedTreeIndex !== null) {
        return child;
      }

      var mapResult = addNodeAtDepthAndIndex({
        targetDepth: targetDepth,
        minimumTreeIndex: minimumTreeIndex,
        newNode: newNode,
        ignoreCollapsed: ignoreCollapsed,
        expandParent: expandParent,
        isLastChild: isLastChild && i === newChildren.length - 1,
        node: child,
        currentIndex: childIndex,
        currentDepth: currentDepth + 1,
        getNodeKey: getNodeKey,
        path: []
      });

      if ('insertedTreeIndex' in mapResult) {
        insertedTreeIndex = mapResult.insertedTreeIndex;
        parentNode = mapResult.parentNode;
        pathFragment = mapResult.parentPath;
      }

      childIndex = mapResult.nextIndex;
      return mapResult.node;
    });
  }

  var nextNode = _extends({}, node, {
    children: newChildren
  });

  var result = {
    node: nextNode,
    nextIndex: childIndex
  };

  if (insertedTreeIndex !== null) {
    result.insertedTreeIndex = insertedTreeIndex;
    result.parentPath = [].concat(selfPath(nextNode), pathFragment);
    result.parentNode = parentNode;
  }

  return result;
};

var insertNode = function insertNode(_ref22) {
  var treeData = _ref22.treeData,
      targetDepth = _ref22.depth,
      minimumTreeIndex = _ref22.minimumTreeIndex,
      newNode = _ref22.newNode,
      getNodeKey = _ref22.getNodeKey,
      _ref22$ignoreCollapse = _ref22.ignoreCollapsed,
      ignoreCollapsed = _ref22$ignoreCollapse === void 0 ? true : _ref22$ignoreCollapse,
      _ref22$expandParent = _ref22.expandParent,
      expandParent = _ref22$expandParent === void 0 ? false : _ref22$expandParent;

  if (!treeData && targetDepth === 0) {
    return {
      treeData: [newNode],
      treeIndex: 0,
      path: [getNodeKey({
        node: newNode,
        treeIndex: 0
      })],
      parentNode: null
    };
  }

  var insertResult = addNodeAtDepthAndIndex({
    targetDepth: targetDepth,
    minimumTreeIndex: minimumTreeIndex,
    newNode: newNode,
    ignoreCollapsed: ignoreCollapsed,
    expandParent: expandParent,
    getNodeKey: getNodeKey,
    isPseudoRoot: true,
    isLastChild: true,
    node: {
      children: treeData
    },
    currentIndex: -1,
    currentDepth: -1
  });

  if (!('insertedTreeIndex' in insertResult)) {
    throw new Error('No suitable position found to insert.');
  }

  var treeIndex = insertResult.insertedTreeIndex;
  return {
    treeData: insertResult.node.children,
    treeIndex: treeIndex,
    path: [].concat(insertResult.parentPath, [getNodeKey({
      node: newNode,
      treeIndex: treeIndex
    })]),
    parentNode: insertResult.parentNode
  };
};
var getFlatDataFromTree = function getFlatDataFromTree(_ref23) {
  var treeData = _ref23.treeData,
      getNodeKey = _ref23.getNodeKey,
      _ref23$ignoreCollapse = _ref23.ignoreCollapsed,
      ignoreCollapsed = _ref23$ignoreCollapse === void 0 ? true : _ref23$ignoreCollapse;

  if (!treeData || treeData.length < 1) {
    return [];
  }

  var flattened = [];
  walk({
    treeData: treeData,
    getNodeKey: getNodeKey,
    ignoreCollapsed: ignoreCollapsed,
    callback: function callback(nodeInfo) {
      flattened.push(nodeInfo);
    }
  });
  return flattened;
};
var getTreeFromFlatData = function getTreeFromFlatData(_ref24) {
  var flatData = _ref24.flatData,
      _ref24$getKey = _ref24.getKey,
      getKey = _ref24$getKey === void 0 ? function (node) {
    return node.id;
  } : _ref24$getKey,
      _ref24$getParentKey = _ref24.getParentKey,
      getParentKey = _ref24$getParentKey === void 0 ? function (node) {
    return node.parentId;
  } : _ref24$getParentKey,
      _ref24$rootKey = _ref24.rootKey,
      rootKey = _ref24$rootKey === void 0 ? '0' : _ref24$rootKey;

  if (!flatData) {
    return [];
  }

  var childrenToParents = {};
  flatData.forEach(function (child) {
    var parentKey = getParentKey(child);

    if (parentKey in childrenToParents) {
      childrenToParents[parentKey].push(child);
    } else {
      childrenToParents[parentKey] = [child];
    }
  });

  if (!(rootKey in childrenToParents)) {
    return [];
  }

  var trav = function trav(parent) {
    var parentKey = getKey(parent);

    if (parentKey in childrenToParents) {
      return _extends({}, parent, {
        children: childrenToParents[parentKey].map(function (child) {
          return trav(child);
        })
      });
    }

    return _extends({}, parent);
  };

  return childrenToParents[rootKey].map(function (child) {
    return trav(child);
  });
};
var isDescendant = function isDescendant(older, younger) {
  return !!older.children && typeof older.children !== 'function' && older.children.some(function (child) {
    return child === younger || isDescendant(child, younger);
  });
};
var getDepth = function getDepth(node, depth) {
  if (depth === void 0) {
    depth = 0;
  }

  if (!node.children) {
    return depth;
  }

  if (typeof node.children === 'function') {
    return depth + 1;
  }

  return node.children.reduce(function (deepest, child) {
    return Math.max(deepest, getDepth(child, depth + 1));
  }, depth);
};
var find = function find(_ref25) {
  var getNodeKey = _ref25.getNodeKey,
      treeData = _ref25.treeData,
      searchQuery = _ref25.searchQuery,
      searchMethod = _ref25.searchMethod,
      searchFocusOffset = _ref25.searchFocusOffset,
      _ref25$expandAllMatch = _ref25.expandAllMatchPaths,
      expandAllMatchPaths = _ref25$expandAllMatch === void 0 ? false : _ref25$expandAllMatch,
      _ref25$expandFocusMat = _ref25.expandFocusMatchPaths,
      expandFocusMatchPaths = _ref25$expandFocusMat === void 0 ? true : _ref25$expandFocusMat;
  var matchCount = 0;

  var trav = function trav(_ref26) {
    var _ref26$isPseudoRoot = _ref26.isPseudoRoot,
        isPseudoRoot = _ref26$isPseudoRoot === void 0 ? false : _ref26$isPseudoRoot,
        node = _ref26.node,
        currentIndex = _ref26.currentIndex,
        _ref26$path = _ref26.path,
        path = _ref26$path === void 0 ? [] : _ref26$path;
    var matches = [];
    var isSelfMatch = false;
    var hasFocusMatch = false;
    var selfPath = isPseudoRoot ? [] : [].concat(path, [getNodeKey({
      node: node,
      treeIndex: currentIndex
    })]);
    var extraInfo = isPseudoRoot ? null : {
      path: selfPath,
      treeIndex: currentIndex
    };
    var hasChildren = node.children && typeof node.children !== 'function' && node.children.length > 0;

    if (!isPseudoRoot && searchMethod(_extends({}, extraInfo, {
      node: node,
      searchQuery: searchQuery
    }))) {
      if (matchCount === searchFocusOffset) {
        hasFocusMatch = true;
      }

      matchCount += 1;
      isSelfMatch = true;
    }

    var childIndex = currentIndex;

    var newNode = _extends({}, node);

    if (hasChildren) {
      newNode.children = newNode.children.map(function (child) {
        var mapResult = trav({
          node: child,
          currentIndex: childIndex + 1,
          path: selfPath
        });

        if (mapResult.node.expanded) {
          childIndex = mapResult.treeIndex;
        } else {
          childIndex += 1;
        }

        if (mapResult.matches.length > 0 || mapResult.hasFocusMatch) {
          matches = [].concat(matches, mapResult.matches);

          if (mapResult.hasFocusMatch) {
            hasFocusMatch = true;
          }

          if (expandAllMatchPaths && mapResult.matches.length > 0 || (expandAllMatchPaths || expandFocusMatchPaths) && mapResult.hasFocusMatch) {
            newNode.expanded = true;
          }
        }

        return mapResult.node;
      });
    }

    if (!isPseudoRoot && !newNode.expanded) {
      matches = matches.map(function (match) {
        return _extends({}, match, {
          treeIndex: null
        });
      });
    }

    if (isSelfMatch) {
      matches = [_extends({}, extraInfo, {
        node: newNode
      })].concat(matches);
    }

    return {
      node: matches.length > 0 ? newNode : node,
      matches: matches,
      hasFocusMatch: hasFocusMatch,
      treeIndex: childIndex
    };
  };

  var result = trav({
    node: {
      children: treeData
    },
    isPseudoRoot: true,
    currentIndex: -1
  });
  return {
    matches: result.matches,
    treeData: result.node.children
  };
};

var _excluded$3 = ["scaffoldBlockPxWidth", "toggleChildrenVisibility", "connectDragPreview", "connectDragSource", "isDragging", "canDrop", "canDrag", "node", "title", "subtitle", "draggedNode", "path", "treeIndex", "isSearchMatch", "isSearchFocus", "buttons", "className", "style", "didDrop", "treeId", "isOver", "parentNode", "rowDirection"];
var defaultProps$2 = {
  isSearchMatch: false,
  isSearchFocus: false,
  canDrag: false,
  toggleChildrenVisibility: undefined,
  buttons: [],
  className: '',
  style: {},
  parentNode: undefined,
  draggedNode: undefined,
  canDrop: false,
  title: undefined,
  subtitle: undefined,
  rowDirection: 'ltr'
};

var NodeRendererDefault = function NodeRendererDefault(props) {
  props = _extends({}, defaultProps$2, props);

  var _props = props,
      scaffoldBlockPxWidth = _props.scaffoldBlockPxWidth,
      toggleChildrenVisibility = _props.toggleChildrenVisibility,
      connectDragPreview = _props.connectDragPreview,
      connectDragSource = _props.connectDragSource,
      isDragging = _props.isDragging,
      canDrop = _props.canDrop,
      canDrag = _props.canDrag,
      node = _props.node,
      title = _props.title,
      subtitle = _props.subtitle,
      draggedNode = _props.draggedNode,
      path = _props.path,
      treeIndex = _props.treeIndex,
      isSearchMatch = _props.isSearchMatch,
      isSearchFocus = _props.isSearchFocus,
      buttons = _props.buttons,
      className = _props.className,
      style = _props.style,
      didDrop = _props.didDrop;
      _props.treeId;
      _props.isOver;
      _props.parentNode;
      var rowDirection = _props.rowDirection,
      otherProps = _objectWithoutPropertiesLoose(_props, _excluded$3);

  var nodeTitle = title || node.title;
  var nodeSubtitle = subtitle || node.subtitle;
  var rowDirectionClass = rowDirection === 'rtl' ? 'rst__rtl' : null;
  var handle;

  if (canDrag) {
    if (typeof node.children === 'function' && node.expanded) {
      handle = jsxRuntime.jsx("div", {
        className: "rst__loadingHandle",
        children: jsxRuntime.jsx("div", {
          className: "rst__loadingCircle",
          children: [].concat(new Array(12)).map(function (_, index) {
            return jsxRuntime.jsx("div", {
              className: classnames('rst__loadingCirclePoint', rowDirectionClass != null ? rowDirectionClass : '')
            }, index);
          })
        }, void 0)
      }, void 0);
    } else {
      handle = connectDragSource(jsxRuntime.jsx("div", {
        className: "rst__moveHandle"
      }, void 0), {
        dropEffect: 'copy'
      });
    }
  }

  var isDraggedDescendant = draggedNode && isDescendant(draggedNode, node);
  var isLandingPadActive = !didDrop && isDragging;
  var buttonStyle = {
    left: -0.5 * scaffoldBlockPxWidth,
    right: 0
  };

  if (rowDirection === 'rtl') {
    buttonStyle = {
      right: -0.5 * scaffoldBlockPxWidth,
      left: 0
    };
  }

  return jsxRuntime.jsxs("div", _extends({
    style: {
      height: '100%'
    }
  }, otherProps, {
    children: [toggleChildrenVisibility && node.children && (node.children.length > 0 || typeof node.children === 'function') && jsxRuntime.jsxs("div", {
      children: [jsxRuntime.jsx("button", {
        type: "button",
        "aria-label": node.expanded ? 'Collapse' : 'Expand',
        className: classnames(node.expanded ? 'rst__collapseButton' : 'rst__expandButton', rowDirectionClass != null ? rowDirectionClass : ''),
        style: buttonStyle,
        onClick: function onClick() {
          return toggleChildrenVisibility({
            node: node,
            path: path,
            treeIndex: treeIndex
          });
        }
      }, void 0), node.expanded && !isDragging && jsxRuntime.jsx("div", {
        style: {
          width: scaffoldBlockPxWidth
        },
        className: classnames('rst__lineChildren', rowDirectionClass != null ? rowDirectionClass : '')
      }, void 0)]
    }, void 0), jsxRuntime.jsx("div", {
      className: classnames('rst__rowWrapper', rowDirectionClass != null ? rowDirectionClass : ''),
      children: connectDragPreview(jsxRuntime.jsxs("div", {
        className: classnames('rst__row', isLandingPadActive ? 'rst__rowLandingPad' : '', isLandingPadActive && !canDrop ? 'rst__rowCancelPad' : '', isSearchMatch ? 'rst__rowSearchMatch' : '', isSearchFocus ? 'rst__rowSearchFocus' : '', rowDirectionClass != null ? rowDirectionClass : '', className != null ? className : ''),
        style: _extends({
          opacity: isDraggedDescendant ? 0.5 : 1
        }, style),
        children: [handle, jsxRuntime.jsxs("div", {
          className: classnames('rst__rowContents', !canDrag ? 'rst__rowContentsDragDisabled' : '', rowDirectionClass != null ? rowDirectionClass : ''),
          children: [jsxRuntime.jsxs("div", {
            className: classnames('rst__rowLabel', rowDirectionClass != null ? rowDirectionClass : ''),
            children: [jsxRuntime.jsx("span", {
              className: classnames('rst__rowTitle', node.subtitle ? 'rst__rowTitleWithSubtitle' : ''),
              children: typeof nodeTitle === 'function' ? nodeTitle({
                node: node,
                path: path,
                treeIndex: treeIndex
              }) : nodeTitle
            }, void 0), nodeSubtitle && jsxRuntime.jsx("span", {
              className: "rst__rowSubtitle",
              children: typeof nodeSubtitle === 'function' ? nodeSubtitle({
                node: node,
                path: path,
                treeIndex: treeIndex
              }) : nodeSubtitle
            }, void 0)]
          }, void 0), jsxRuntime.jsx("div", {
            className: "rst__rowToolbar",
            children: buttons == null ? void 0 : buttons.map(function (btn, index) {
              return jsxRuntime.jsx("div", {
                className: "rst__toolbarButton",
                children: btn
              }, index);
            })
          }, void 0)]
        }, void 0)]
      }, void 0))
    }, void 0)]
  }), void 0);
};

var NodeRendererDefault$1 = NodeRendererDefault;

var defaultProps$1 = {
  isOver: false,
  canDrop: false
};

var PlaceholderRendererDefault = function PlaceholderRendererDefault(props) {
  props = _extends({}, defaultProps$1, props);
  var _props = props,
      canDrop = _props.canDrop,
      isOver = _props.isOver;
  return jsxRuntime.jsx("div", {
    className: classnames('rst__placeholder', canDrop ? 'rst__placeholderLandingPad' : '', canDrop && !isOver ? 'rst__placeholderCancelPad' : '')
  }, void 0);
};

var PlaceholderRendererDefault$1 = PlaceholderRendererDefault;

var _excluded$2 = ["children", "listIndex", "swapFrom", "swapLength", "swapDepth", "scaffoldBlockPxWidth", "lowerSiblingCounts", "connectDropTarget", "isOver", "draggedNode", "canDrop", "treeIndex", "treeId", "getPrevRow", "node", "path", "rowDirection"];

var TreeNode = function (_Component) {
  _inheritsLoose(TreeNode, _Component);

  function TreeNode() {
    return _Component.apply(this, arguments) || this;
  }

  var _proto = TreeNode.prototype;

  _proto.render = function render() {
    var _this$props = this.props,
        children = _this$props.children,
        listIndex = _this$props.listIndex,
        _this$props$swapFrom = _this$props.swapFrom,
        swapFrom = _this$props$swapFrom === void 0 ? undefined : _this$props$swapFrom,
        _this$props$swapLengt = _this$props.swapLength,
        swapLength = _this$props$swapLengt === void 0 ? undefined : _this$props$swapLengt,
        _this$props$swapDepth = _this$props.swapDepth,
        swapDepth = _this$props$swapDepth === void 0 ? undefined : _this$props$swapDepth,
        scaffoldBlockPxWidth = _this$props.scaffoldBlockPxWidth,
        lowerSiblingCounts = _this$props.lowerSiblingCounts,
        connectDropTarget = _this$props.connectDropTarget,
        isOver = _this$props.isOver,
        _this$props$draggedNo = _this$props.draggedNode,
        draggedNode = _this$props$draggedNo === void 0 ? undefined : _this$props$draggedNo,
        _this$props$canDrop = _this$props.canDrop,
        canDrop = _this$props$canDrop === void 0 ? false : _this$props$canDrop,
        treeIndex = _this$props.treeIndex;
        _this$props.treeId;
        _this$props.getPrevRow;
        _this$props.node;
        _this$props.path;
        var _this$props$rowDirect = _this$props.rowDirection,
        rowDirection = _this$props$rowDirect === void 0 ? 'ltr' : _this$props$rowDirect,
        otherProps = _objectWithoutPropertiesLoose(_this$props, _excluded$2);

    var rowDirectionClass = rowDirection === 'rtl' ? 'rst__rtl' : null;
    var scaffoldBlockCount = lowerSiblingCounts.length;
    var scaffold = [];
    lowerSiblingCounts.forEach(function (lowerSiblingCount, i) {
      var lineClass = '';

      if (lowerSiblingCount > 0) {
        if (listIndex === 0) {
          lineClass = 'rst__lineHalfHorizontalRight rst__lineHalfVerticalBottom';
        } else if (i === scaffoldBlockCount - 1) {
          lineClass = 'rst__lineHalfHorizontalRight rst__lineFullVertical';
        } else {
          lineClass = 'rst__lineFullVertical';
        }
      } else if (listIndex === 0) {
        lineClass = 'rst__lineHalfHorizontalRight';
      } else if (i === scaffoldBlockCount - 1) {
        lineClass = 'rst__lineHalfVerticalTop rst__lineHalfHorizontalRight';
      }

      scaffold.push(jsxRuntime.jsx("div", {
        style: {
          width: scaffoldBlockPxWidth
        },
        className: classnames('rst__lineBlock', lineClass, rowDirectionClass != null ? rowDirectionClass : '')
      }, "pre_" + (1 + i)));

      if (treeIndex !== listIndex && i === swapDepth) {
        var highlightLineClass = '';

        if (listIndex === swapFrom + swapLength - 1) {
          highlightLineClass = 'rst__highlightBottomLeftCorner';
        } else if (treeIndex === swapFrom) {
          highlightLineClass = 'rst__highlightTopLeftCorner';
        } else {
          highlightLineClass = 'rst__highlightLineVertical';
        }

        var _style;

        if (rowDirection === 'rtl') {
          _style = {
            width: scaffoldBlockPxWidth,
            right: scaffoldBlockPxWidth * i
          };
        } else {
          _style = {
            width: scaffoldBlockPxWidth,
            left: scaffoldBlockPxWidth * i
          };
        }

        scaffold.push(jsxRuntime.jsx("div", {
          style: _style,
          className: classnames('rst__absoluteLineBlock', highlightLineClass, rowDirectionClass != null ? rowDirectionClass : '')
        }, i));
      }
    });
    var style;

    if (rowDirection === 'rtl') {
      style = {
        right: scaffoldBlockPxWidth * scaffoldBlockCount
      };
    } else {
      style = {
        left: scaffoldBlockPxWidth * scaffoldBlockCount
      };
    }

    return connectDropTarget(jsxRuntime.jsxs("div", _extends({}, otherProps, {
      className: classnames('rst__node', rowDirectionClass != null ? rowDirectionClass : ''),
      children: [scaffold, jsxRuntime.jsx("div", {
        className: "rst__nodeContent",
        style: style,
        children: React.Children.map(children, function (child) {
          return React.cloneElement(child, {
            isOver: isOver,
            canDrop: canDrop,
            draggedNode: draggedNode
          });
        })
      }, void 0)]
    }), void 0));
  };

  return TreeNode;
}(React.Component);

var TreeNode$1 = TreeNode;

var _excluded$1 = ["children", "connectDropTarget", "treeId", "drop"];
var defaultProps = {
  canDrop: false,
  draggedNode: null
};

var TreePlaceholder = function TreePlaceholder(props) {
  props = _extends({}, defaultProps, props);

  var _props = props,
      children = _props.children,
      connectDropTarget = _props.connectDropTarget;
      _props.treeId;
      _props.drop;
      var otherProps = _objectWithoutPropertiesLoose(_props, _excluded$1);

  return connectDropTarget(jsxRuntime.jsx("div", {
    children: React.Children.map(children, function (child) {
      return React.cloneElement(child, _extends({}, otherProps));
    })
  }, void 0));
};

var TreePlaceholder$1 = TreePlaceholder;

var defaultGetNodeKey = function defaultGetNodeKey(_ref) {
  var treeIndex = _ref.treeIndex;
  return treeIndex;
};

var getReactElementText = function getReactElementText(parent) {
  if (typeof parent === 'string') {
    return parent;
  }

  if (parent === null || typeof parent !== 'object' || !parent.props || !parent.props.children || typeof parent.props.children !== 'string' && typeof parent.props.children !== 'object') {
    return '';
  }

  if (typeof parent.props.children === 'string') {
    return parent.props.children;
  }

  return parent.props.children.map(function (child) {
    return getReactElementText(child);
  }).join('');
};

var stringSearch = function stringSearch(key, searchQuery, node, path, treeIndex) {
  if (typeof node[key] === 'function') {
    return String(node[key]({
      node: node,
      path: path,
      treeIndex: treeIndex
    })).indexOf(searchQuery) > -1;
  }

  if (typeof node[key] === 'object') {
    return getReactElementText(node[key]).indexOf(searchQuery) > -1;
  }

  return node[key] && String(node[key]).indexOf(searchQuery) > -1;
};

var defaultSearchMethod = function defaultSearchMethod(_ref2) {
  var node = _ref2.node,
      path = _ref2.path,
      treeIndex = _ref2.treeIndex,
      searchQuery = _ref2.searchQuery;
  return stringSearch('title', searchQuery, node, path, treeIndex) || stringSearch('subtitle', searchQuery, node, path, treeIndex);
};

var memoize = function memoize(f) {
  var savedArgsArray = [];
  var savedKeysArray = [];
  var savedResult = null;
  return function (args) {
    var keysArray = Object.keys(args).sort();
    var argsArray = keysArray.map(function (key) {
      return args[key];
    });

    if (argsArray.length !== savedArgsArray.length || argsArray.some(function (arg, index) {
      return arg !== savedArgsArray[index];
    }) || keysArray.some(function (key, index) {
      return key !== savedKeysArray[index];
    })) {
      savedArgsArray = argsArray;
      savedKeysArray = keysArray;
      savedResult = f(args);
    }

    return savedResult;
  };
};

var memoizedInsertNode = memoize(insertNode);
var memoizedGetFlatDataFromTree = memoize(getFlatDataFromTree);
var memoizedGetDescendantCount = memoize(getDescendantCount);

var rafId = 0;
var wrapSource = function wrapSource(el, startDrag, _endDrag, dndType) {
  var nodeDragSource = {
    beginDrag: function beginDrag(props) {
      startDrag(props);
      return {
        node: props.node,
        parentNode: props.parentNode,
        path: props.path,
        treeIndex: props.treeIndex,
        treeId: props.treeId
      };
    },
    endDrag: function endDrag(props, monitor) {
      _endDrag(monitor.getDropResult());
    },
    isDragging: function isDragging(props, monitor) {
      var dropTargetNode = monitor.getItem().node;
      var draggedNode = props.node;
      return draggedNode === dropTargetNode;
    }
  };

  var nodeDragSourcePropInjection = function nodeDragSourcePropInjection(connect, monitor) {
    return {
      connectDragSource: connect.dragSource(),
      connectDragPreview: connect.dragPreview(),
      isDragging: monitor.isDragging(),
      didDrop: monitor.didDrop()
    };
  };

  return reactDnd.DragSource(dndType, nodeDragSource, nodeDragSourcePropInjection)(el);
};
var wrapPlaceholder = function wrapPlaceholder(el, treeId, _drop, dndType) {
  var placeholderDropTarget = {
    drop: function drop(dropTargetProps, monitor) {
      var _monitor$getItem = monitor.getItem(),
          node = _monitor$getItem.node,
          path = _monitor$getItem.path,
          treeIndex = _monitor$getItem.treeIndex;

      var result = {
        node: node,
        path: path,
        treeIndex: treeIndex,
        treeId: treeId,
        minimumTreeIndex: 0,
        depth: 0
      };

      _drop(result);

      return result;
    }
  };

  var placeholderPropInjection = function placeholderPropInjection(connect, monitor) {
    var dragged = monitor.getItem();
    return {
      connectDropTarget: connect.dropTarget(),
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
      draggedNode: dragged ? dragged.node : null
    };
  };

  return reactDnd.DropTarget(dndType, placeholderDropTarget, placeholderPropInjection)(el);
};

var getTargetDepth = function getTargetDepth(dropTargetProps, monitor, component, canNodeHaveChildren, treeId, maxDepth) {
  var dropTargetDepth = 0;
  var rowAbove = dropTargetProps.getPrevRow();

  if (rowAbove) {
    var path = rowAbove.path;
    var aboveNodeCannotHaveChildren = !canNodeHaveChildren(rowAbove.node);

    if (aboveNodeCannotHaveChildren) {
      path = path.slice(0, path.length - 1);
    }

    dropTargetDepth = Math.min(path.length, dropTargetProps.path.length);
  }

  var blocksOffset;
  var dragSourceInitialDepth = (monitor.getItem().path || []).length;

  if (monitor.getItem().treeId !== treeId) {
    dragSourceInitialDepth = 0;

    if (component) {
      var _findDOMNode;

      var relativePosition = (_findDOMNode = reactDom.findDOMNode(component)) == null ? void 0 : _findDOMNode.getBoundingClientRect();
      var leftShift = monitor.getSourceClientOffset().x - relativePosition.left;
      blocksOffset = Math.round(leftShift / dropTargetProps.scaffoldBlockPxWidth);
    } else {
      blocksOffset = dropTargetProps.path.length;
    }
  } else {
    var direction = dropTargetProps.rowDirection === 'rtl' ? -1 : 1;
    blocksOffset = Math.round(direction * monitor.getDifferenceFromInitialOffset().x / dropTargetProps.scaffoldBlockPxWidth);
  }

  var targetDepth = Math.min(dropTargetDepth, Math.max(0, dragSourceInitialDepth + blocksOffset - 1));

  if (typeof maxDepth !== 'undefined' && maxDepth !== null) {
    var draggedNode = monitor.getItem().node;
    var draggedChildDepth = getDepth(draggedNode);
    targetDepth = Math.max(0, Math.min(targetDepth, maxDepth - draggedChildDepth - 1));
  }

  return targetDepth;
};

var _canDrop = function canDrop(dropTargetProps, monitor, canNodeHaveChildren, treeId, maxDepth, treeRefcanDrop, draggingTreeData, treeReftreeData, getNodeKey) {
  if (!monitor.isOver()) {
    return false;
  }

  var rowAbove = dropTargetProps.getPrevRow();
  var abovePath = rowAbove ? rowAbove.path : [];
  var aboveNode = rowAbove ? rowAbove.node : {};
  var targetDepth = getTargetDepth(dropTargetProps, monitor, null, canNodeHaveChildren, treeId, maxDepth);

  if (targetDepth >= abovePath.length && typeof aboveNode.children === 'function') {
    return false;
  }

  if (typeof treeRefcanDrop === 'function') {
    var _monitor$getItem2 = monitor.getItem(),
        node = _monitor$getItem2.node;

    var addedResult = memoizedInsertNode({
      treeData: draggingTreeData || treeReftreeData,
      newNode: node,
      depth: targetDepth,
      getNodeKey: getNodeKey,
      minimumTreeIndex: dropTargetProps.listIndex,
      expandParent: true
    });
    return treeRefcanDrop({
      node: node,
      prevPath: monitor.getItem().path,
      prevParent: monitor.getItem().parentNode,
      prevTreeIndex: monitor.getItem().treeIndex,
      nextPath: addedResult.path,
      nextParent: addedResult.parentNode,
      nextTreeIndex: addedResult.treeIndex
    });
  }

  return true;
};

var wrapTarget = function wrapTarget(el, canNodeHaveChildren, treeId, maxDepth, treeRefcanDrop, _drop2, dragHover, dndType, draggingTreeData, treeReftreeData, getNodeKey) {
  var nodeDropTarget = {
    drop: function drop(dropTargetProps, monitor, component) {
      var result = {
        node: monitor.getItem().node,
        path: monitor.getItem().path,
        treeIndex: monitor.getItem().treeIndex,
        treeId: treeId,
        minimumTreeIndex: dropTargetProps.treeIndex,
        depth: getTargetDepth(dropTargetProps, monitor, component, canNodeHaveChildren, treeId, maxDepth)
      };

      _drop2(result);

      return result;
    },
    hover: function hover(dropTargetProps, monitor, component) {
      var targetDepth = getTargetDepth(dropTargetProps, monitor, component, canNodeHaveChildren, treeId, maxDepth);
      var draggedNode = monitor.getItem().node;
      var needsRedraw = dropTargetProps.node !== draggedNode || targetDepth !== dropTargetProps.path.length - 1;

      if (!needsRedraw) {
        return;
      }

      cancelAnimationFrame(rafId);
      rafId = requestAnimationFrame(function () {
        var item = monitor.getItem();

        if (!item || !monitor.isOver()) {
          return;
        }

        dragHover({
          node: draggedNode,
          path: item.path,
          minimumTreeIndex: dropTargetProps.listIndex,
          depth: targetDepth
        });
      });
    },
    canDrop: function canDrop(dropTargetProps, monitor) {
      return _canDrop(dropTargetProps, monitor, canNodeHaveChildren, treeId, maxDepth, treeRefcanDrop, draggingTreeData, treeReftreeData, getNodeKey);
    }
  };

  var nodeDropTargetPropInjection = function nodeDropTargetPropInjection(connect, monitor) {
    var dragged = monitor.getItem();
    return {
      connectDropTarget: connect.dropTarget(),
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
      draggedNode: dragged ? dragged.node : null
    };
  };

  return reactDnd.DropTarget(dndType, nodeDropTarget, nodeDropTargetPropInjection)(el);
};

var slideRows = function slideRows(rows, fromIndex, toIndex, count) {
  if (count === void 0) {
    count = 1;
  }

  var rowsWithoutMoved = [].concat(rows.slice(0, fromIndex), rows.slice(fromIndex + count));
  return [].concat(rowsWithoutMoved.slice(0, toIndex), rows.slice(fromIndex, fromIndex + count), rowsWithoutMoved.slice(toIndex));
};

var _excluded = ["dragDropManager"];
var treeIdCounter = 1;

var mergeTheme = function mergeTheme(props) {
  var merged = _extends({}, props, {
    style: _extends({}, props.theme.style, props.style),
    innerStyle: _extends({}, props.theme.innerStyle, props.innerStyle)
  });

  var overridableDefaults = {
    nodeContentRenderer: NodeRendererDefault$1,
    placeholderRenderer: PlaceholderRendererDefault$1,
    scaffoldBlockPxWidth: 44,
    slideRegionSize: 100,
    treeNodeRenderer: TreeNode$1
  };
  Object.keys(overridableDefaults).forEach(function (propKey) {
    if (props[propKey] === null) {
      merged[propKey] = typeof props.theme[propKey] !== 'undefined' ? props.theme[propKey] : overridableDefaults[propKey];
    }
  });
  return merged;
};

var ReactSortableTree = function (_Component) {
  _inheritsLoose(ReactSortableTree, _Component);

  function ReactSortableTree(props) {
    var _this;

    _this = _Component.call(this, props) || this;

    _this.startDrag = function (_ref) {
      var path = _ref.path;

      _this.setState(function (prevState) {
        var _removeNode = removeNode({
          treeData: prevState.instanceProps.treeData,
          path: path,
          getNodeKey: _this.props.getNodeKey
        }),
            draggingTreeData = _removeNode.treeData,
            draggedNode = _removeNode.node,
            draggedMinimumTreeIndex = _removeNode.treeIndex;

        return {
          draggingTreeData: draggingTreeData,
          draggedNode: draggedNode,
          draggedDepth: path.length - 1,
          draggedMinimumTreeIndex: draggedMinimumTreeIndex,
          dragging: true
        };
      });
    };

    _this.dragHover = function (_ref2) {
      var draggedNode = _ref2.node,
          draggedDepth = _ref2.depth,
          draggedMinimumTreeIndex = _ref2.minimumTreeIndex;

      if (_this.state.draggedDepth === draggedDepth && _this.state.draggedMinimumTreeIndex === draggedMinimumTreeIndex) {
        return;
      }

      _this.setState(function (_ref3) {
        var draggingTreeData = _ref3.draggingTreeData,
            instanceProps = _ref3.instanceProps;
        var newDraggingTreeData = draggingTreeData || instanceProps.treeData;
        var addedResult = memoizedInsertNode({
          treeData: newDraggingTreeData,
          newNode: draggedNode,
          depth: draggedDepth,
          minimumTreeIndex: draggedMinimumTreeIndex,
          expandParent: true,
          getNodeKey: _this.props.getNodeKey
        });

        var rows = _this.getRows(addedResult.treeData);

        var expandedParentPath = rows[addedResult.treeIndex].path;
        return {
          draggedNode: draggedNode,
          draggedDepth: draggedDepth,
          draggedMinimumTreeIndex: draggedMinimumTreeIndex,
          draggingTreeData: changeNodeAtPath({
            treeData: newDraggingTreeData,
            path: expandedParentPath.slice(0, -1),
            newNode: function newNode(_ref4) {
              var node = _ref4.node;
              return _extends({}, node, {
                expanded: true
              });
            },
            getNodeKey: _this.props.getNodeKey
          }),
          searchFocusTreeIndex: null,
          dragging: true
        };
      });
    };

    _this.endDrag = function (dropResult) {
      var instanceProps = _this.state.instanceProps;

      var resetTree = function resetTree() {
        return _this.setState({
          draggingTreeData: null,
          draggedNode: null,
          draggedMinimumTreeIndex: null,
          draggedDepth: null,
          dragging: false
        });
      };

      if (!dropResult) {
        resetTree();
      } else if (dropResult.treeId !== _this.treeId) {
        var node = dropResult.node,
            path = dropResult.path,
            treeIndex = dropResult.treeIndex;
        var shouldCopy = _this.props.shouldCopyOnOutsideDrop;

        if (typeof shouldCopy === 'function') {
          shouldCopy = shouldCopy({
            node: node,
            prevTreeIndex: treeIndex,
            prevPath: path
          });
        }

        var treeData = _this.state.draggingTreeData || instanceProps.treeData;

        if (shouldCopy) {
          treeData = changeNodeAtPath({
            treeData: instanceProps.treeData,
            path: path,
            newNode: function newNode(_ref5) {
              var copyNode = _ref5.node;
              return _extends({}, copyNode);
            },
            getNodeKey: _this.props.getNodeKey
          });
        }

        _this.props.onChange(treeData);

        _this.props.onMoveNode({
          treeData: treeData,
          node: node,
          treeIndex: null,
          path: null,
          nextPath: null,
          nextTreeIndex: null,
          prevPath: path,
          prevTreeIndex: treeIndex
        });
      }
    };

    _this.drop = function (dropResult) {
      _this.moveNode(dropResult);
    };

    _this.canNodeHaveChildren = function (node) {
      var canNodeHaveChildren = _this.props.canNodeHaveChildren;

      if (canNodeHaveChildren) {
        return canNodeHaveChildren(node);
      }

      return true;
    };

    _this.listRef = React__default["default"].createRef();

    var _mergeTheme = mergeTheme(props),
        dndType = _mergeTheme.dndType,
        nodeContentRenderer = _mergeTheme.nodeContentRenderer,
        treeNodeRenderer = _mergeTheme.treeNodeRenderer,
        slideRegionSize = _mergeTheme.slideRegionSize;

    _this.treeId = "rst__" + treeIdCounter;
    treeIdCounter += 1;
    _this.dndType = dndType || _this.treeId;
    _this.nodeContentRenderer = wrapSource(nodeContentRenderer, _this.startDrag, _this.endDrag, _this.dndType);
    _this.treePlaceholderRenderer = wrapPlaceholder(TreePlaceholder$1, _this.treeId, _this.drop, _this.dndType);
    _this.scrollZoneVirtualList = (withScrolling.createScrollingComponent || withScrolling__default["default"])(React__default["default"].forwardRef(function (props, ref) {
      props.dragDropManager;
          var otherProps = _objectWithoutPropertiesLoose(props, _excluded);

      return jsxRuntime.jsx(reactVirtuoso.Virtuoso, _extends({
        ref: _this.listRef
      }, otherProps), void 0);
    }));
    _this.vStrength = withScrolling.createVerticalStrength(slideRegionSize);
    _this.hStrength = withScrolling.createHorizontalStrength(slideRegionSize);
    _this.state = {
      draggingTreeData: null,
      draggedNode: null,
      draggedMinimumTreeIndex: null,
      draggedDepth: null,
      searchMatches: [],
      searchFocusTreeIndex: null,
      dragging: false,
      instanceProps: {
        treeData: [],
        ignoreOneTreeUpdate: false,
        searchQuery: null,
        searchFocusOffset: null
      }
    };
    _this.treeNodeRenderer = wrapTarget(treeNodeRenderer, _this.canNodeHaveChildren, _this.treeId, _this.props.maxDepth, _this.props.canDrop, _this.drop, _this.dragHover, _this.dndType, _this.state.draggingTreeData, _this.props.treeData, _this.props.getNodeKey);
    _this.toggleChildrenVisibility = _this.toggleChildrenVisibility.bind(_assertThisInitialized(_this));
    _this.moveNode = _this.moveNode.bind(_assertThisInitialized(_this));
    _this.startDrag = _this.startDrag.bind(_assertThisInitialized(_this));
    _this.dragHover = _this.dragHover.bind(_assertThisInitialized(_this));
    _this.endDrag = _this.endDrag.bind(_assertThisInitialized(_this));
    _this.drop = _this.drop.bind(_assertThisInitialized(_this));
    _this.handleDndMonitorChange = _this.handleDndMonitorChange.bind(_assertThisInitialized(_this));
    return _this;
  }

  var _proto = ReactSortableTree.prototype;

  _proto.componentDidMount = function componentDidMount() {
    ReactSortableTree.loadLazyChildren(this.props, this.state);
    var stateUpdate = ReactSortableTree.search(this.props, this.state, true, true, false);
    this.setState(stateUpdate);
    this.clearMonitorSubscription = this.props.dragDropManager.getMonitor().subscribeToStateChange(this.handleDndMonitorChange);
  };

  ReactSortableTree.getDerivedStateFromProps = function getDerivedStateFromProps(nextProps, prevState) {
    var instanceProps = prevState.instanceProps;
    var newState = {};
    var isTreeDataEqual = isEqual__default["default"](instanceProps.treeData, nextProps.treeData);
    instanceProps.treeData = nextProps.treeData;

    if (!isTreeDataEqual) {
      if (instanceProps.ignoreOneTreeUpdate) {
        instanceProps.ignoreOneTreeUpdate = false;
      } else {
        newState.searchFocusTreeIndex = null;
        ReactSortableTree.loadLazyChildren(nextProps, prevState);
        Object.assign(newState, ReactSortableTree.search(nextProps, prevState, false, false, false));
      }

      newState.draggingTreeData = null;
      newState.draggedNode = null;
      newState.draggedMinimumTreeIndex = null;
      newState.draggedDepth = null;
      newState.dragging = false;
    } else if (!isEqual__default["default"](instanceProps.searchQuery, nextProps.searchQuery)) {
      Object.assign(newState, ReactSortableTree.search(nextProps, prevState, true, true, false));
    } else if (instanceProps.searchFocusOffset !== nextProps.searchFocusOffset) {
      Object.assign(newState, ReactSortableTree.search(nextProps, prevState, true, true, true));
    }

    instanceProps.searchQuery = nextProps.searchQuery;
    instanceProps.searchFocusOffset = nextProps.searchFocusOffset;
    newState.instanceProps = _extends({}, instanceProps, newState.instanceProps);
    return newState;
  };

  _proto.componentDidUpdate = function componentDidUpdate(prevProps, prevState) {
    if (this.state.dragging !== prevState.dragging) {
      if (this.props.onDragStateChanged) {
        this.props.onDragStateChanged({
          isDragging: this.state.dragging,
          draggedNode: this.state.draggedNode
        });
      }
    }
  };

  _proto.componentWillUnmount = function componentWillUnmount() {
    this.clearMonitorSubscription();
  };

  _proto.getRows = function getRows(treeData) {
    return memoizedGetFlatDataFromTree({
      ignoreCollapsed: true,
      getNodeKey: this.props.getNodeKey,
      treeData: treeData
    });
  };

  _proto.handleDndMonitorChange = function handleDndMonitorChange() {
    var _this2 = this;

    var monitor = this.props.dragDropManager.getMonitor();

    if (!monitor.isDragging() && this.state.draggingTreeData) {
      setTimeout(function () {
        _this2.endDrag();
      });
    }
  };

  _proto.toggleChildrenVisibility = function toggleChildrenVisibility(_ref6) {
    var targetNode = _ref6.node,
        path = _ref6.path;
    var instanceProps = this.state.instanceProps;
    var treeData = changeNodeAtPath({
      treeData: instanceProps.treeData,
      path: path,
      newNode: function newNode(_ref7) {
        var node = _ref7.node;
        return _extends({}, node, {
          expanded: !node.expanded
        });
      },
      getNodeKey: this.props.getNodeKey
    });
    this.props.onChange(treeData);
    this.props.onVisibilityToggle({
      treeData: treeData,
      node: targetNode,
      expanded: !targetNode.expanded,
      path: path
    });
  };

  _proto.moveNode = function moveNode(_ref8) {
    var node = _ref8.node,
        prevPath = _ref8.path,
        prevTreeIndex = _ref8.treeIndex,
        depth = _ref8.depth,
        minimumTreeIndex = _ref8.minimumTreeIndex;

    var _insertNode = insertNode({
      treeData: this.state.draggingTreeData,
      newNode: node,
      depth: depth,
      minimumTreeIndex: minimumTreeIndex,
      expandParent: true,
      getNodeKey: this.props.getNodeKey
    }),
        treeData = _insertNode.treeData,
        treeIndex = _insertNode.treeIndex,
        path = _insertNode.path,
        nextParentNode = _insertNode.parentNode;

    this.props.onChange(treeData);
    this.props.onMoveNode({
      treeData: treeData,
      node: node,
      treeIndex: treeIndex,
      path: path,
      nextPath: path,
      nextTreeIndex: treeIndex,
      prevPath: prevPath,
      prevTreeIndex: prevTreeIndex,
      nextParentNode: nextParentNode
    });
  };

  ReactSortableTree.search = function search(props, state, seekIndex, expand, singleSearch) {
    var onChange = props.onChange,
        getNodeKey = props.getNodeKey,
        searchFinishCallback = props.searchFinishCallback,
        searchQuery = props.searchQuery,
        searchMethod = props.searchMethod,
        searchFocusOffset = props.searchFocusOffset,
        onlyExpandSearchedNodes = props.onlyExpandSearchedNodes;
    var instanceProps = state.instanceProps;

    if (!searchQuery && !searchMethod) {
      if (searchFinishCallback) {
        searchFinishCallback([]);
      }

      return {
        searchMatches: []
      };
    }

    var newState = {
      instanceProps: {}
    };

    var _find = find({
      getNodeKey: getNodeKey,
      treeData: onlyExpandSearchedNodes ? toggleExpandedForAll({
        treeData: instanceProps.treeData,
        expanded: false
      }) : instanceProps.treeData,
      searchQuery: searchQuery,
      searchMethod: searchMethod || defaultSearchMethod,
      searchFocusOffset: searchFocusOffset,
      expandAllMatchPaths: expand && !singleSearch,
      expandFocusMatchPaths: !!expand
    }),
        expandedTreeData = _find.treeData,
        searchMatches = _find.matches;

    if (expand) {
      newState.instanceProps.ignoreOneTreeUpdate = true;
      onChange(expandedTreeData);
    }

    if (searchFinishCallback) {
      searchFinishCallback(searchMatches);
    }

    var searchFocusTreeIndex = null;

    if (seekIndex && searchFocusOffset !== null && searchFocusOffset < searchMatches.length) {
      searchFocusTreeIndex = searchMatches[searchFocusOffset].treeIndex;
    }

    newState.searchMatches = searchMatches;
    newState.searchFocusTreeIndex = searchFocusTreeIndex;
    return newState;
  };

  ReactSortableTree.loadLazyChildren = function loadLazyChildren(props, state) {
    var instanceProps = state.instanceProps;
    walk({
      treeData: instanceProps.treeData,
      getNodeKey: props.getNodeKey,
      callback: function callback(_ref9) {
        var node = _ref9.node,
            path = _ref9.path,
            lowerSiblingCounts = _ref9.lowerSiblingCounts,
            treeIndex = _ref9.treeIndex;

        if (node.children && typeof node.children === 'function' && (node.expanded || props.loadCollapsedLazyChildren)) {
          node.children({
            node: node,
            path: path,
            lowerSiblingCounts: lowerSiblingCounts,
            treeIndex: treeIndex,
            done: function done(childrenArray) {
              return props.onChange(changeNodeAtPath({
                treeData: instanceProps.treeData,
                path: path,
                newNode: function newNode(_ref10) {
                  var oldNode = _ref10.node;
                  return oldNode === node ? _extends({}, oldNode, {
                    children: childrenArray
                  }) : oldNode;
                },
                getNodeKey: props.getNodeKey
              }));
            }
          });
        }
      }
    });
  };

  _proto.renderRow = function renderRow(row, _ref11) {
    var listIndex = _ref11.listIndex,
        style = _ref11.style,
        getPrevRow = _ref11.getPrevRow,
        matchKeys = _ref11.matchKeys,
        swapFrom = _ref11.swapFrom,
        swapDepth = _ref11.swapDepth,
        swapLength = _ref11.swapLength;
    var node = row.node,
        parentNode = row.parentNode,
        path = row.path,
        lowerSiblingCounts = row.lowerSiblingCounts,
        treeIndex = row.treeIndex;

    var _mergeTheme2 = mergeTheme(this.props),
        canDrag = _mergeTheme2.canDrag,
        generateNodeProps = _mergeTheme2.generateNodeProps,
        scaffoldBlockPxWidth = _mergeTheme2.scaffoldBlockPxWidth,
        searchFocusOffset = _mergeTheme2.searchFocusOffset,
        rowDirection = _mergeTheme2.rowDirection;

    var TreeNodeRenderer = this.treeNodeRenderer;
    var NodeContentRenderer = this.nodeContentRenderer;
    var nodeKey = path[path.length - 1];
    var isSearchMatch = (nodeKey in matchKeys);
    var isSearchFocus = isSearchMatch && matchKeys[nodeKey] === searchFocusOffset;
    var callbackParams = {
      node: node,
      parentNode: parentNode,
      path: path,
      lowerSiblingCounts: lowerSiblingCounts,
      treeIndex: treeIndex,
      isSearchMatch: isSearchMatch,
      isSearchFocus: isSearchFocus
    };
    var nodeProps = !generateNodeProps ? {} : generateNodeProps(callbackParams);
    var rowCanDrag = typeof canDrag !== 'function' ? canDrag : canDrag(callbackParams);
    var sharedProps = {
      treeIndex: treeIndex,
      scaffoldBlockPxWidth: scaffoldBlockPxWidth,
      node: node,
      path: path,
      treeId: this.treeId,
      rowDirection: rowDirection
    };
    return jsxRuntime.jsx(TreeNodeRenderer, _extends({
      style: style,
      listIndex: listIndex,
      getPrevRow: getPrevRow,
      lowerSiblingCounts: lowerSiblingCounts,
      swapFrom: swapFrom,
      swapLength: swapLength,
      swapDepth: swapDepth
    }, sharedProps, {
      children: jsxRuntime.jsx(NodeContentRenderer, _extends({
        parentNode: parentNode,
        isSearchMatch: isSearchMatch,
        isSearchFocus: isSearchFocus,
        canDrag: rowCanDrag,
        toggleChildrenVisibility: this.toggleChildrenVisibility
      }, sharedProps, nodeProps), void 0)
    }), nodeKey);
  };

  _proto.render = function render() {
    var _this3 = this;

    var _mergeTheme3 = mergeTheme(this.props),
        dragDropManager = _mergeTheme3.dragDropManager,
        style = _mergeTheme3.style,
        className = _mergeTheme3.className,
        innerStyle = _mergeTheme3.innerStyle,
        placeholderRenderer = _mergeTheme3.placeholderRenderer,
        getNodeKey = _mergeTheme3.getNodeKey,
        rowDirection = _mergeTheme3.rowDirection;

    var _this$state = this.state,
        searchMatches = _this$state.searchMatches,
        searchFocusTreeIndex = _this$state.searchFocusTreeIndex,
        draggedNode = _this$state.draggedNode,
        draggedDepth = _this$state.draggedDepth,
        draggedMinimumTreeIndex = _this$state.draggedMinimumTreeIndex,
        instanceProps = _this$state.instanceProps;
    var treeData = this.state.draggingTreeData || instanceProps.treeData;
    var rowDirectionClass = rowDirection === 'rtl' ? 'rst__rtl' : null;
    var rows;
    var swapFrom = null;
    var swapLength = null;

    if (draggedNode && draggedMinimumTreeIndex !== null) {
      var addedResult = memoizedInsertNode({
        treeData: treeData,
        newNode: draggedNode,
        depth: draggedDepth,
        minimumTreeIndex: draggedMinimumTreeIndex,
        expandParent: true,
        getNodeKey: getNodeKey
      });
      var swapTo = draggedMinimumTreeIndex;
      swapFrom = addedResult.treeIndex;
      swapLength = 1 + memoizedGetDescendantCount({
        node: draggedNode
      });
      rows = slideRows(this.getRows(addedResult.treeData), swapFrom, swapTo, swapLength);
    } else {
      rows = this.getRows(treeData);
    }

    var matchKeys = {};
    searchMatches.forEach(function (_ref12, i) {
      var path = _ref12.path;
      matchKeys[path[path.length - 1]] = i;
    });

    if (searchFocusTreeIndex !== null) {
      this.listRef.current.scrollToIndex({
        index: searchFocusTreeIndex,
        align: 'center'
      });
    }

    var containerStyle = style;
    var list;

    if (rows.length < 1) {
      var Placeholder = this.treePlaceholderRenderer;
      var PlaceholderContent = placeholderRenderer;
      list = jsxRuntime.jsx(Placeholder, {
        treeId: this.treeId,
        drop: this.drop,
        children: jsxRuntime.jsx(PlaceholderContent, {}, void 0)
      }, void 0);
    } else {
      containerStyle = _extends({
        height: '100%'
      }, containerStyle);
      var ScrollZoneVirtualList = this.scrollZoneVirtualList;
      list = jsxRuntime.jsx(ScrollZoneVirtualList, {
        data: rows,
        dragDropManager: dragDropManager,
        verticalStrength: this.vStrength,
        horizontalStrength: this.hStrength,
        className: "rst__virtualScrollOverride",
        style: innerStyle,
        itemContent: function itemContent(index) {
          return _this3.renderRow(rows[index], {
            listIndex: index,
            getPrevRow: function getPrevRow() {
              return rows[index - 1] || null;
            },
            matchKeys: matchKeys,
            swapFrom: swapFrom,
            swapDepth: draggedDepth,
            swapLength: swapLength
          });
        }
      }, void 0);
    }

    return jsxRuntime.jsx("div", {
      className: classnames('rst__tree', className, rowDirectionClass),
      style: containerStyle,
      children: list
    }, void 0);
  };

  return ReactSortableTree;
}(React.Component);

ReactSortableTree.defaultProps = {
  canDrag: true,
  canDrop: null,
  canNodeHaveChildren: function canNodeHaveChildren() {
    return true;
  },
  className: '',
  dndType: null,
  generateNodeProps: null,
  getNodeKey: defaultGetNodeKey,
  innerStyle: {},
  maxDepth: null,
  treeNodeRenderer: null,
  nodeContentRenderer: null,
  onMoveNode: function onMoveNode() {},
  onVisibilityToggle: function onVisibilityToggle() {},
  placeholderRenderer: null,
  scaffoldBlockPxWidth: null,
  searchFinishCallback: null,
  searchFocusOffset: null,
  searchMethod: null,
  searchQuery: null,
  shouldCopyOnOutsideDrop: false,
  slideRegionSize: null,
  style: {},
  theme: {},
  onDragStateChanged: function onDragStateChanged() {},
  onlyExpandSearchedNodes: false,
  rowDirection: 'ltr',
  debugMode: false
};

var SortableTreeWithoutDndContext = function SortableTreeWithoutDndContext(props) {
  return jsxRuntime.jsx(reactDnd.DndContext.Consumer, {
    children: function children(_ref13) {
      var dragDropManager = _ref13.dragDropManager;
      return dragDropManager === undefined ? null : jsxRuntime.jsx(ReactSortableTree, _extends({}, props, {
        dragDropManager: dragDropManager
      }), void 0);
    }
  }, void 0);
};

var SortableTree = function SortableTree(props) {
  return jsxRuntime.jsx(reactDnd.DndProvider, {
    debugMode: props.debugMode,
    backend: reactDndHtml5Backend.HTML5Backend,
    children: jsxRuntime.jsx(SortableTreeWithoutDndContext, _extends({}, props), void 0)
  }, void 0);
};
var SortableTree$1 = SortableTree;

exports.SortableTreeWithoutDndContext = SortableTreeWithoutDndContext;
exports.addNodeUnderParent = addNodeUnderParent;
exports.changeNodeAtPath = changeNodeAtPath;
exports["default"] = SortableTree$1;
exports.defaultGetNodeKey = defaultGetNodeKey;
exports.defaultSearchMethod = defaultSearchMethod;
exports.find = find;
exports.getDepth = getDepth;
exports.getDescendantCount = getDescendantCount;
exports.getFlatDataFromTree = getFlatDataFromTree;
exports.getNodeAtPath = getNodeAtPath;
exports.getTreeFromFlatData = getTreeFromFlatData;
exports.getVisibleNodeCount = getVisibleNodeCount;
exports.getVisibleNodeInfoAtIndex = getVisibleNodeInfoAtIndex;
exports.insertNode = insertNode;
exports.isDescendant = isDescendant;
exports.map = map;
exports.removeNode = removeNode;
exports.removeNodeAtPath = removeNodeAtPath;
exports.toggleExpandedForAll = toggleExpandedForAll;
exports.walk = walk;
