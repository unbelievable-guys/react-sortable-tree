import { FullTree, GetNodeKeyFunction, NodeData, SearchData, TreeIndex, TreeItem, TreeNode, TreePath } from '..';
export declare type WalkAndMapFunctionParameters = FullTree & {
    getNodeKey: GetNodeKeyFunction;
    callback: Function;
    ignoreCollapsed?: boolean | undefined;
};
export interface FlatDataItem extends TreeNode, TreePath, TreeIndex {
    lowerSiblingCounts: number[];
    parentNode: TreeItem;
}
export declare const getDescendantCount: ({ node, ignoreCollapsed, }: TreeNode & {
    ignoreCollapsed?: boolean | undefined;
}) => number;
/**
 * Count all the visible (expanded) descendants in the tree data.
 *
 * @param {!Object[]} treeData - Tree data
 *
 * @return {number} count
 */
export declare const getVisibleNodeCount: ({ treeData }: FullTree) => number;
/**
 * Get the <targetIndex>th visible node in the tree data.
 *
 * @param {!Object[]} treeData - Tree data
 * @param {!number} targetIndex - The index of the node to search for
 * @param {!function} getNodeKey - Function to get the key from the nodeData and tree index
 *
 * @return {{
 *      node: Object,
 *      path: []string|[]number,
 *      lowerSiblingCounts: []number
 *  }|null} node - The node at targetIndex, or null if not found
 */
export declare const getVisibleNodeInfoAtIndex: ({ treeData, index: targetIndex, getNodeKey, }: FullTree & {
    index: number;
    getNodeKey: GetNodeKeyFunction;
}) => (TreeNode & TreePath & {
    lowerSiblingCounts: number[];
}) | null;
/**
 * Walk descendants depth-first and call a callback on each
 *
 * @param {!Object[]} treeData - Tree data
 * @param {!function} getNodeKey - Function to get the key from the nodeData and tree index
 * @param {function} callback - Function to call on each node
 * @param {boolean=} ignoreCollapsed - Ignore children of nodes without `expanded` set to `true`
 *
 * @return void
 */
export declare const walk: ({ treeData, getNodeKey, callback, ignoreCollapsed, }: WalkAndMapFunctionParameters) => void;
/**
 * Perform a depth-first transversal of the descendants and
 *  make a change to every node in the tree
 *
 * @param {!Object[]} treeData - Tree data
 * @param {!function} getNodeKey - Function to get the key from the nodeData and tree index
 * @param {function} callback - Function to call on each node
 * @param {boolean=} ignoreCollapsed - Ignore children of nodes without `expanded` set to `true`
 *
 * @return {Object[]} changedTreeData - The changed tree data
 */
export declare const map: ({ treeData, getNodeKey, callback, ignoreCollapsed, }: WalkAndMapFunctionParameters) => TreeItem[];
/**
 * Expand or close every node in the tree
 *
 * @param {!Object[]} treeData - Tree data
 * @param {?boolean} expanded - Whether the node is expanded or not
 *
 * @return {Object[]} changedTreeData - The changed tree data
 */
export declare const toggleExpandedForAll: ({ treeData, expanded, }: FullTree & {
    expanded?: boolean | undefined;
}) => TreeItem[];
/**
 * Replaces node at path with object, or callback-defined object
 *
 * @param {!Object[]} treeData
 * @param {number[]|string[]} path - Array of keys leading up to node to be changed
 * @param {function|any} newNode - Node to replace the node at the path with, or a function producing the new node
 * @param {!function} getNodeKey - Function to get the key from the nodeData and tree index
 * @param {boolean=} ignoreCollapsed - Ignore children of nodes without `expanded` set to `true`
 *
 * @return {Object[]} changedTreeData - The changed tree data
 */
export declare const changeNodeAtPath: ({ treeData, path, newNode, getNodeKey, ignoreCollapsed, }: FullTree & TreePath & {
    newNode: Function | any;
    getNodeKey: GetNodeKeyFunction;
    ignoreCollapsed?: boolean | undefined;
}) => TreeItem[];
/**
 * Removes the node at the specified path and returns the resulting treeData.
 *
 * @param {!Object[]} treeData
 * @param {number[]|string[]} path - Array of keys leading up to node to be deleted
 * @param {!function} getNodeKey - Function to get the key from the nodeData and tree index
 * @param {boolean=} ignoreCollapsed - Ignore children of nodes without `expanded` set to `true`
 *
 * @return {Object[]} changedTreeData - The tree data with the node removed
 */
export declare const removeNodeAtPath: ({ treeData, path, getNodeKey, ignoreCollapsed, }: FullTree & TreePath & {
    getNodeKey: GetNodeKeyFunction;
    ignoreCollapsed?: boolean | undefined;
}) => TreeItem[];
/**
 * Removes the node at the specified path and returns the resulting treeData.
 *
 * @param {!Object[]} treeData
 * @param {number[]|string[]} path - Array of keys leading up to node to be deleted
 * @param {!function} getNodeKey - Function to get the key from the nodeData and tree index
 * @param {boolean=} ignoreCollapsed - Ignore children of nodes without `expanded` set to `true`
 *
 * @return {Object} result
 * @return {Object[]} result.treeData - The tree data with the node removed
 * @return {Object} result.node - The node that was removed
 * @return {number} result.treeIndex - The previous treeIndex of the removed node
 */
export declare const removeNode: ({ treeData, path, getNodeKey, ignoreCollapsed, }: FullTree & TreePath & {
    getNodeKey: GetNodeKeyFunction;
    ignoreCollapsed?: boolean | undefined;
}) => (FullTree & TreeNode & TreeIndex) | null;
/**
 * Gets the node at the specified path
 *
 * @param {!Object[]} treeData
 * @param {number[]|string[]} path - Array of keys leading up to node to be deleted
 * @param {!function} getNodeKey - Function to get the key from the nodeData and tree index
 * @param {boolean=} ignoreCollapsed - Ignore children of nodes without `expanded` set to `true`
 *
 * @return {Object|null} nodeInfo - The node info at the given path, or null if not found
 */
export declare const getNodeAtPath: ({ treeData, path, getNodeKey, ignoreCollapsed, }: FullTree & TreePath & {
    getNodeKey: GetNodeKeyFunction;
    ignoreCollapsed?: boolean | undefined;
}) => (TreeNode & TreeIndex) | null;
/**
 * Adds the node to the specified parent and returns the resulting treeData.
 *
 * @param {!Object[]} treeData
 * @param {!Object} newNode - The node to insert
 * @param {number|string} parentKey - The key of the to-be parentNode of the node
 * @param {!function} getNodeKey - Function to get the key from the nodeData and tree index
 * @param {boolean=} ignoreCollapsed - Ignore children of nodes without `expanded` set to `true`
 * @param {boolean=} expandParent - If true, expands the parentNode specified by parentPath
 * @param {boolean=} addAsFirstChild - If true, adds new node as first child of tree
 *
 * @return {Object} result
 * @return {Object[]} result.treeData - The updated tree data
 * @return {number} result.treeIndex - The tree index at which the node was inserted
 */
export declare const addNodeUnderParent: ({ treeData, newNode, parentKey, getNodeKey, ignoreCollapsed, expandParent, addAsFirstChild, }: FullTree & {
    newNode: TreeItem;
    parentKey?: number | string | undefined | null;
    getNodeKey: GetNodeKeyFunction;
    ignoreCollapsed?: boolean | undefined;
    expandParent?: boolean | undefined;
    addAsFirstChild?: boolean | undefined;
}) => FullTree & TreeIndex;
/**
 * Insert a node into the tree at the given depth, after the minimum index
 *
 * @param {!Object[]} treeData - Tree data
 * @param {!number} depth - The depth to insert the node at (the first level of the array being depth 0)
 * @param {!number} minimumTreeIndex - The lowest possible treeIndex to insert the node at
 * @param {!Object} newNode - The node to insert into the tree
 * @param {boolean=} ignoreCollapsed - Ignore children of nodes without `expanded` set to `true`
 * @param {boolean=} expandParent - If true, expands the parent of the inserted node
 * @param {!function} getNodeKey - Function to get the key from the nodeData and tree index
 *
 * @return {Object} result
 * @return {Object[]} result.treeData - The tree data with the node added
 * @return {number} result.treeIndex - The tree index at which the node was inserted
 * @return {number[]|string[]} result.path - Array of keys leading to the node location after insertion
 * @return {Object} result.parentNode - The parent node of the inserted node
 */
export declare const insertNode: ({ treeData, depth: targetDepth, minimumTreeIndex, newNode, getNodeKey, ignoreCollapsed, expandParent, }: FullTree & {
    depth: number;
    newNode: TreeItem;
    minimumTreeIndex: number;
    ignoreCollapsed?: boolean | undefined;
    expandParent?: boolean | undefined;
    getNodeKey: GetNodeKeyFunction;
}) => FullTree & TreeIndex & TreePath & {
    parentNode: TreeItem | null;
};
/**
 * Get tree data flattened.
 *
 * @param {!Object[]} treeData - Tree data
 * @param {!function} getNodeKey - Function to get the key from the nodeData and tree index
 * @param {boolean=} ignoreCollapsed - Ignore children of nodes without `expanded` set to `true`
 *
 * @return {{
 *      node: Object,
 *      path: []string|[]number,
 *      lowerSiblingCounts: []number
 *  }}[] nodes - The node array
 */
export declare const getFlatDataFromTree: ({ treeData, getNodeKey, ignoreCollapsed, }: FullTree & {
    getNodeKey: GetNodeKeyFunction;
    ignoreCollapsed?: boolean | undefined;
}) => FlatDataItem[];
/**
 * Generate a tree structure from flat data.
 *
 * @param {!Object[]} flatData
 * @param {!function=} getKey - Function to get the key from the nodeData
 * @param {!function=} getParentKey - Function to get the parent key from the nodeData
 * @param {string|number=} rootKey - The value returned by `getParentKey` that corresponds to the root node.
 *                                  For example, if your nodes have id 1-99, you might use rootKey = 0
 *
 * @return {Object[]} treeData - The flat data represented as a tree
 */
export declare const getTreeFromFlatData: ({ flatData, getKey, getParentKey, rootKey, }: {
    flatData: any;
    getKey?: ((node: any) => any) | undefined;
    getParentKey?: ((node: any) => any) | undefined;
    rootKey?: string | undefined;
}) => any;
/**
 * Check if a node is a descendant of another node.
 *
 * @param {!Object} older - Potential ancestor of younger node
 * @param {!Object} younger - Potential descendant of older node
 *
 * @return {boolean}
 */
export declare const isDescendant: (older: TreeItem, younger: TreeItem) => boolean;
/**
 * Get the maximum depth of the children (the depth of the root node is 0).
 *
 * @param {!Object} node - Node in the tree
 * @param {?number} depth - The current depth
 *
 * @return {number} maxDepth - The deepest depth in the tree
 */
export declare const getDepth: (node: TreeItem, depth?: number | undefined) => number;
/**
 * Find nodes matching a search query in the tree,
 *
 * @param {!function} getNodeKey - Function to get the key from the nodeData and tree index
 * @param {!Object[]} treeData - Tree data
 * @param {?string|number} searchQuery - Function returning a boolean to indicate whether the node is a match or not
 * @param {!function} searchMethod - Function returning a boolean to indicate whether the node is a match or not
 * @param {?number} searchFocusOffset - The offset of the match to focus on
 *                                      (e.g., 0 focuses on the first match, 1 on the second)
 * @param {boolean=} expandAllMatchPaths - If true, expands the paths to any matched node
 * @param {boolean=} expandFocusMatchPaths - If true, expands the path to the focused node
 *
 * @return {Object[]} matches - An array of objects containing the matching `node`s, their `path`s and `treeIndex`s
 * @return {Object[]} treeData - The original tree data with all relevant nodes expanded.
 *                               If expandAllMatchPaths and expandFocusMatchPaths are both false,
 *                               it will be the same as the original tree data.
 */
export declare const find: ({ getNodeKey, treeData, searchQuery, searchMethod, searchFocusOffset, expandAllMatchPaths, expandFocusMatchPaths, }: FullTree & {
    getNodeKey: GetNodeKeyFunction;
    searchQuery?: string | number | undefined;
    searchMethod: (data: SearchData) => boolean;
    searchFocusOffset?: number | undefined;
    expandAllMatchPaths?: boolean | undefined;
    expandFocusMatchPaths?: boolean | undefined;
}) => {
    matches: NodeData[];
} & FullTree;
