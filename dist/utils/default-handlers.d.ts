import { ReactNode } from 'react';
export interface GetTreeItemChildren {
    done: (children: TreeItem[]) => void;
    node: TreeItem;
    path: number[];
    lowerSiblingCounts: number[];
    treeIndex: number;
}
export declare type GetTreeItemChildrenFn = (data: GetTreeItemChildren) => void;
export declare type GetNodeKeyFunction = (data: TreeIndex & TreeNode) => string | number;
export interface TreeItem {
    title?: ReactNode | undefined;
    subtitle?: ReactNode | undefined;
    expanded?: boolean | undefined;
    children?: TreeItem[] | GetTreeItemChildrenFn | undefined;
    [key: string]: any;
}
export interface TreeNode {
    node: TreeItem;
}
export interface TreePath {
    path: number[];
}
export interface TreeIndex {
    treeIndex: number;
}
export interface FullTree {
    treeData: TreeItem[] | undefined | null;
}
export interface NodeData extends TreeNode, TreePath, TreeIndex {
}
export interface SearchData extends NodeData {
    searchQuery: string;
}
export declare type SearchParams = {
    node: TreeItem;
    path: number[];
    treeIndex: number;
    searchQuery: string;
};
export declare type SearchFinishCallbackParams = {
    node: TreeItem;
    path: number[];
    treeIndex: number;
}[];
export declare type GenerateNodePropsParams = {
    node: TreeItem;
    path: number[];
    treeIndex: number;
    lowerSiblingCounts: number[];
    isSearchMatch: boolean;
    isSearchFocus: boolean;
};
export declare type ShouldCopyOnOutsideDropParams = {
    node: any;
    prevPath: number[];
    prevTreeIndex: number;
};
export declare type OnMoveNodeParams = {
    treeData: TreeItem[];
    node: TreeItem;
    nextParentNode?: TreeItem;
    prevPath: number[];
    prevTreeIndex: number;
    nextPath: number[] | null;
    nextTreeIndex: number | null;
};
export declare type CanDropParams = {
    node: TreeItem;
    prevPath: number[];
    prevParent: TreeItem | null;
    prevTreeIndex: number;
    nextPath: number[];
    nextParent: TreeItem | null;
    nextTreeIndex: number;
};
export declare type OnVisibilityToggleParams = {
    treeData: TreeItem[];
    node: TreeItem;
    expanded: boolean;
    path: number[];
};
export declare type OnDragStateChangedParams = {
    isDragging: boolean;
    draggedNode: TreeItem;
};
export declare type ReactSortableTreeProps = {
    dragDropManager?: {
        getMonitor: () => unknown;
    };
    treeData: any[];
    style?: {
        [key: string]: any;
    };
    className?: string;
    innerStyle?: {
        [key: string]: any;
    };
    slideRegionSize?: number;
    scaffoldBlockPxWidth?: number;
    maxDepth?: number;
    searchMethod?: (params: SearchParams) => boolean;
    searchQuery?: string;
    searchFocusOffset?: number;
    searchFinishCallback?: (params: SearchFinishCallbackParams) => void;
    generateNodeProps?: (params: GenerateNodePropsParams) => any;
    treeNodeRenderer?: any;
    nodeContentRenderer?: any;
    placeholderRenderer?: any;
    theme?: {
        style: any;
        innerStyle: any;
        scaffoldBlockPxWidth: number;
        slideRegionSize: number;
        treeNodeRenderer: any;
        nodeContentRenderer: any;
        placeholderRenderer: any;
    };
    getNodeKey?: GetNodeKeyFunction;
    onChange: (treeData: TreeItem[]) => void;
    onMoveNode?: (params: OnMoveNodeParams) => void;
    canDrag?: (params: GenerateNodePropsParams) => boolean;
    canDrop?: (params: CanDropParams) => boolean;
    canNodeHaveChildren?: (node: TreeItem) => boolean;
    shouldCopyOnOutsideDrop?: (params: ShouldCopyOnOutsideDropParams) => boolean | boolean;
    onVisibilityToggle?: (params: OnVisibilityToggleParams) => void;
    dndType?: string;
    onDragStateChanged?: (params: OnDragStateChangedParams) => void;
    onlyExpandSearchedNodes?: boolean;
    rowDirection?: 'rtl' | 'ltr';
    debugMode?: boolean;
};
export declare const defaultGetNodeKey: ({ treeIndex }: TreeIndex) => number;
export declare const defaultSearchMethod: ({ node, path, treeIndex, searchQuery, }: SearchData) => boolean;
