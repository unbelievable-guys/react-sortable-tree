import React, { Component } from 'react';
import { ConnectDropTarget } from 'react-dnd';
import './tree-node.css';
import { TreeItem, TreeNode as TreeNodeI, TreePath } from '.';
export interface FlatDataItem extends TreeNodeI, TreePath {
    lowerSiblingCounts: number[];
    parentNode: TreeItem;
}
export interface TreeRendererProps {
    treeIndex: number;
    treeId: string;
    swapFrom?: number | undefined;
    swapDepth?: number | undefined;
    swapLength?: number | undefined;
    scaffoldBlockPxWidth: number;
    lowerSiblingCounts: number[];
    rowDirection?: 'ltr' | 'rtl' | string | undefined;
    listIndex: number;
    children: JSX.Element[];
    style?: React.CSSProperties | undefined;
    connectDropTarget: ConnectDropTarget;
    isOver: boolean;
    canDrop?: boolean | undefined;
    draggedNode?: TreeItem | undefined;
    getPrevRow: () => FlatDataItem | null;
    node: TreeItem;
    path: number[];
}
declare class TreeNode extends Component<TreeRendererProps> {
    render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | null;
}
export default TreeNode;
